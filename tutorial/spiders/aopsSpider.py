import scrapy
import os
import urlparse
import time
from scrapy.http import Request

hasht = {}
class aopsSpider(scrapy.Spider):
    name = "aops"
    allowed_domains = ["artofproblemsolving.com"]
    start_urls = [
        "http://artofproblemsolving.com/wiki/index.php?title=Category:Math_Contest_Problems"
    ]
    global type2url
    type2url = []
    type2url.append('http://artofproblemsolving.com/wiki/index.php?title=2005_Alabama_ARML_TST')
    type2url.append('http://artofproblemsolving.com/wiki/index.php?title=2006_Alabama_ARML_TST')
    type2url.append('http://artofproblemsolving.com/wiki/index.php?title=2007_Alabama_ARML_TST')
    global type3url
    type3url = []
    type3url.append('http://artofproblemsolving.com/wiki/index.php?title=Cyprus_Juniors_Provincial_Problems_and_Solutions')
    type3url.append('http://artofproblemsolving.com/wiki/index.php?title=Cyprus_Juniors_TST_Problems_and_Solutions')
    type3url.append('http://artofproblemsolving.com/wiki/index.php?title=Cyprus_MO_Problems_and_Solutions')
    type3url.append('http://artofproblemsolving.com/wiki/index.php?title=Cyprus_Seniors_Provincial_Problems_and_Solutions')
    type3url.append('http://artofproblemsolving.com/wiki/index.php?title=Seniors_Pancyprian_Problems_and_Solutions')
    type3url.append('http://artofproblemsolving.com/wiki/index.php?title=Stanford_Mathematics_Tournament_Problems')
    global hasht
    hasht['http://artofproblemsolving.com/wiki/index.php?title=Category:Math_Contest_Problems'] = 1
    hasht['http://artofproblemsolving.com/wiki/index.php?title=User:Btilm305/tocheck2'] = 1
    global foldname
    foldname = time.strftime("%Y-%m-%d %X", time.localtime())
    foldname = foldname.replace(':', '-')
    os.mkdir(foldname)
    #os.chdir(foldname)
    def subparse(self, response):
        if hasht.has_key(response.url):
            return
        hasht[response.url] = 1;
        nowlevel = response.url.count('/')
        #print nowlevel
        delta = response.meta['delta']
        adr = response.meta['adr']
        if adr.count('/') > 5 + delta:
            return
        suitnamer = response.url.replace('=', '/');
        suitname = suitnamer.split('/')[-1]
        os.mkdir(adr + '/' + suitname)
        #os.chdir(suitname)
        #print 'in', response.url
        if nowlevel == 5 + delta:
            file = open(adr + '/' + suitname.replace(':', '_') + '\\html.txt', "w")
            opt = response.xpath('//*[@id="main-column"]/div[2]').extract();
            #print "?",opt
            file.write(''.join(opt).encode('utf8'));
            file.write('\n');
            file.close()
        else:
            for sel in response.xpath('//ul/li'):
                if sel.xpath('a/class').extract() == "new":
                    print "remved!"
                    continue;
                #print "?", sel.xpath('a/@href').extract()
                if sel.xpath('a/@href').extract() == []:
                    continue;
                s = sel.xpath('a/@href').extract()[0]
                if s[0] == '#':
                    continue
                ural2 = urlparse.urljoin(response.url, sel.xpath('a/@href').extract()[0]);
                nextlevel = ural2.count('/')
                #print nextlevel, nowlevel
                if adr.count('/') == 0 and nextlevel == 5 + delta:
                    continue
                if (nextlevel > nowlevel) or (nextlevel == nowlevel and adr.count('/') == 0):
                    #print "sub look at!!!", ural2
                    yield Request(ural2, meta={'adr': adr + '/' + suitname, 'delta' : delta}, callback=self.subparse)
        
    def parse(self, response):
        print response.url
        for sel in response.xpath('//ul/li'):
            s = sel.xpath('a/@href').extract()[0]
            if s[0] == '#':
                continue
            ural2 = urlparse.urljoin(response.url, sel.xpath('a/@href').extract()[0]);
            global foldname
            if ural2 in type2url:
                continue
            elif ural2 in type3url:
                yield Request(ural2, meta={'adr': foldname, 'delta': 1}, callback=self.subparse)
            else:
                #continue
                yield Request(ural2, meta={'adr': foldname, 'delta': 0}, callback=self.subparse)
            